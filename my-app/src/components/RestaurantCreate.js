import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import NavBar from './NavBar';

class RestaurantCreate extends Component {
    constructor() {
        super();
        this.state = {
            name: null,
            email: null,
            address: null,
            rating: null,
        }
    }

    handleSubmit() {
        fetch('http://localhost:3000/resturant', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then((response) => {
            response.json()
                .then((result) => {
                    alert("Resturant Added Thank You!!!")
                })
        })
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div>
                <NavBar />
                <h1>Restaurant Create</h1>
                <input type="text" value={this.state.name} onChange={this.handleChange} placeholder="Enter resturant Name" name="name"/>
                <br /> <br />
                <input type="text" value={this.state.email} onChange={this.handleChange} placeholder="Enter resturant email-id" name="email"/>
                <br /> <br />
                <input type="text" value={this.state.address} onChange={this.handleChange} placeholder="Enter location" name="address"/>
                <br /> <br />
                <input type="text" value={this.state.rating} onChange={this.handleChange} placeholder="Enter Ratings" name="rating"/>
                <br /><br />
                <Button variant="outline-success" onClick={() => {this.handleSubmit()}}>Submit</Button>{' '}
            </div>
        );
    }
}

export default RestaurantCreate;