import React, { Component } from 'react';
import { Table, Form, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import NavBar from './NavBar';

class RestaurantSearch extends Component {
    constructor() {
        super();
        this.state = {
            searchData: null,
            noData: false,
            lastSearch: ""
        }
    }

    handleChange(key) {
        console.log(key);
        fetch('http://localhost:3000/resturant?q=' + key)
            .then((response) => {
                response.json()
                    .then((result) => {
                        if (result.length > 0) {
                            this.setState({
                                searchData: result,
                                noData: false
                            })
                        }
                        else {
                            this.setState({
                                noData: true,
                                searchData: null,
                            })
                        }
                    })
            })
    }

    handleDelete(id) {
        fetch('http://localhost:3000/resturant/'+id, {
            method: "DELETE",
        }).then((response) => {
            response.json()
                .then((result) => {
                    alert("Resturant is Deleted.")
                    this.handleChange(this.state.lastSearch);
                })
        })
    }

    render() {
        return (
            <Container>
                <NavBar />
                <h1>Restaurant Search</h1>
                <Form.Control type="text" style={{ "marginBottom": "20px" }}  onChange={(e) => this.handleChange(e.target.value)} placeholder="Search Resturant" />
                {
                    this.state.searchData ?
                        <div>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email-id</th>
                                        <th>Ratings</th>
                                        <th>location</th>
                                        <th>Operation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.searchData.map((item) =>
                                            <tr>
                                                <td>{item.id}</td>
                                                <td>{item.name}</td>
                                                <td>{item.email}</td>
                                                <td>{item.rating}</td>
                                                <td>{item.address}</td>
                                                <td><Link to={"/update/" + item.id}><FontAwesomeIcon icon={faEdit} /> </Link>
                                                    <span style={{ "marginLeft": "10px" }} onClick={() => this.handleDelete(item.id)}><FontAwesomeIcon icon={faTrashAlt} color="red" /> </span></td>
                                            </tr>
                                        )
                                    }
                                </tbody>
                            </Table>
                        </div>
                        : ""
                }
                {this.state.noData ? <p>No Data Found</p> : null}
            </Container>
        );
    }
}

export default RestaurantSearch;