import React, { Component } from 'react';
import { Form, Container, Button } from 'react-bootstrap';
import NavBar from './NavBar';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            userName: "",
            password: ""
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit() {
        console.log(this.state);
        fetch('http://localhost:3000/login?q=' + this.state.userName)
            .then((response) => {
                response.json()
                    .then((result) => {
                        if (result.length > 0) {
                            localStorage.setItem('login', JSON.stringify(result));
                            this.props.history.push('list');
                        }
                        else {
                            alert('Please Check the userName and Password.');
                        }
                    })
            })
    }

    render() {
        return (
            <div>
                <NavBar />
                <input type="text" style={{ "marginTop": "20px", "width": "22%" }} value={this.state.name} onChange={this.handleChange} placeholder="Enter UserName" name="userName" /> <br />
                <input type="password" style={{ "marginTop": "20px", "width": "22%" }} value={this.state.name} onChange={this.handleChange} placeholder="Enter Password" name="password" /> <br /> <br />
                <Button variant="primary" onClick={() => { this.handleSubmit() }}>Login</Button>{' '}
            </div>
        );
    }
}

export default Login;