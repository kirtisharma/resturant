import React, { Component } from 'react';
import {Table, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import NavBar from './NavBar';

class RestaurantList extends Component {
    constructor() {
        super();
        this.state = {
            list: null,
        }
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        fetch("http://localhost:3000/resturant")
            .then((response) => {
                response.json()
                    .then((result) => {
                        this.setState({
                            list: result,
                        })
                    })
            })
    }

    handleDelete(id) {
        fetch('http://localhost:3000/resturant/'+id, {
            method: "DELETE",
        }).then((response) => {
            response.json()
                .then((result) => {
                    alert("Resturant is Deleted.")
                    this.getData();
                })
        })
    }

    render() {
        console.log(this.state);
        return (
            <div>
                <NavBar />
                <h1>Restaurant List</h1>
                {
                    this.state.list ?
                        <div>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email-id</th>
                                        <th>Ratings</th>
                                        <th>location</th>
                                        <th>Operation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.list.map((item, i) =>
                                            <tr>
                                                <td>{item.id}</td>
                                                <td>{item.name}</td>
                                                <td>{item.email}</td>
                                                <td>{item.rating}</td>
                                                <td>{item.address}</td>
                                                <td><Link to={"/update/"+item.id}><FontAwesomeIcon icon={faEdit} /> </Link> 
                                                <span style={{ "marginLeft": "10px" }} onClick={() => this.handleDelete(item.id)}><FontAwesomeIcon icon={faTrashAlt} color="red" /> </span></td>
                                            </tr>)
                                    }
                                </tbody>
                            </Table>
                        </div>
                        : <p>Please wait...</p>
                }
            </div>
        );
    }
}

export default RestaurantList;