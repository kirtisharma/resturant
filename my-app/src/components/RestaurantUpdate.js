import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import NavBar from './NavBar';

class RestaurantUpdate extends Component {
    constructor() {
        super();
        this.state = {
            name: null,
            email: null,
            address: null,
            rating: null,
            id: null
        }
    }

    componentDidMount() {
        fetch('http://localhost:3000/resturant/'+this.props.match.params.id)
        .then((response) => {
            response.json()
            .then((result) => {
                this.setState({
                    name: result.name,
                    email: result.email,
                    address: result.address,
                    rating: result.rating,
                    id: result.id
                })
            })
        })
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit() {
        fetch('http://localhost:3000/resturant/'+this.state.id, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then((response) => {
            response.json()
                .then((result) => {
                    alert("Resturant has be updated")
                })
        })
    }

    render() {
        console.log(this.props.match.params.id)
        return (
            <div>
                <NavBar />
                <h1>Restaurant Update</h1>
                <input type="text" value={this.state.name} onChange={this.handleChange} placeholder="Enter resturant Name" name="name"/>
                <br /> <br />
                <input type="text" value={this.state.email} onChange={this.handleChange} placeholder="Enter resturant email-id" name="email"/>
                <br /> <br />
                <input type="text" value={this.state.address} onChange={this.handleChange} placeholder="Enter location" name="address"/>
                <br /> <br />
                <input type="text" value={this.state.rating} onChange={this.handleChange} placeholder="Enter Ratings" name="rating"/>
                <br /><br />
                <Button variant="outline-success" onClick={() => {this.handleSubmit()}}><Link to="/list">Resturant Update</Link></Button>{' '}
            </div>
        );
    }
}

export default RestaurantUpdate;