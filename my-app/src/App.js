import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import Home from './components/Home';
import RestaurantCreate from './components/RestaurantCreate';
import RestaurantDetail from './components/RestaurantDetail';
import RestaurantList from './components/RestaurantList';
import RestaurantSearch from './components/RestaurantSearch';
import RestaurantUpdate from './components/RestaurantUpdate';
import Login from './components/Login';
import Protected from './components/Protected';
import Logout from './components/Logout';
import Doughnut from './components/Doughnut'
function App() {
  return (
    <div className="App">
      <Router>

        <Protected exact path="/" component={Home} />
        <Protected path="/create" component={RestaurantCreate} />
        <Protected path="/list" component={RestaurantList} />
        <Protected path="/search" component={RestaurantSearch} />
        <Protected path="/update/:id" component={RestaurantUpdate} />
        <Protected path ="/detail" component={RestaurantDetail} />
        
        {/* <Route path="/update/:id"
          render={props => (
            <RestaurantUpdate {...props} />
          )}
        >
        </Route> */}

        <Route path="/login"
          render={props => (
            <Login {...props} />
          )}
        >
        </Route>
        
        <Route path="/logout">
          <Logout />
        </Route>
      </Router>
    </div>
  );
}

export default App;
